# Mail-Man


An app for sending millions of emails as cheaply as possible. Mail-Man uses AWS Simple Email Service to send bulk emails at $0.10 per 1000 emails.

Mail-Man is fast and memory efficient, sending over 100 emails per second on a 1gb Digital Ocean VPS.

Mail-Man is Good to deliver newsletters to hundreds of thousands of people.


## What does Mail-Man do??

- Send email campaigns of unlimited size.
- Import emails saved in CSV format.
- Create templates to reuse for convenience when sending email campaigns.
- Track bounce rate and other standard metrics. You can also insert tracking pixels and unsubcribe links a click of a button.
- Add custom fields to imported email lists such as names or cities.
- Grant other users (limited or otherwise) permissions to use your account on your behalf.
- Add embedded HTML newsletter sign up forms to your site. These are snippets of code that will let your users sign up with you at the click of a button.

### Performance

Currently sending weekly email blasts of over 700,000 emails in 3-4 hours on a $10 per month Digital Ocean VPS with 1gb memory and 1 core processor.

Mail-Man is fast and scales to the rate limit enforced by AWS.


### How to install

Mail-Man uses lots of different services (redis, Postgres and so on) that would make manual installation arduous. To streamline this process, we use Docker.

### Your machine

Mail-Man is designed to be memory efficient, nonetheless the amount of memory needed will depend on how many emails you are capable of sending per second (this limited is placed by Amazon). I **strongly suggest you enable swap space** if your VPS does not enable this automatically (such as DigitalOcean).

A machine with at least 2gb memory is recommended.

### Installing with Docker

The first step is to download Docker itself. The process for this differs depending your OS. You can find a guide here https://docs.docker.com/engine/installation/.

Now, clone the repository and change into it.

After this, you'll need to create your own .env file. Check out the .env.example file in this repo. From the terminal, you can run `cp .env.example .env` then edit the .env file with any editor of your choice. There are instruction in this file that you can follow.

Now run `docker-compose up`. This will run all the containers needed to launch this app, and will take some time to finish.

When the process is finished, the app will be exposed on port 80 and accessible by visiting `http://[hostname]`.

### Installation summary

1. Install and run the Docker daemon.
2. Clone the repository and change into it `git clone https://github.com/freeCodeCamp/Mail-for-Good && cd Mail-for-Good`.
3. Run `cp .env.example .env` then open `.env` and edit it, passing in your own values.
4. Run `docker-compose up`. Wait for it to finish.
5. Visit `http://[hostname]` or `localhost` if running locally.


BSD 3-Clause License
